const otpGenerator=require("otp-generator");
const services=require("../../services/index");
const model=require("../../models/index");
const hashed=require('../../services/hash');
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");


async function sendotp(req,res,next){
    console.log(req.body)
    const user=await model.adminModel.findOne({phone: req.body.phone})
    if(user)
    return res.status(400).send("user already");
    const OTP=otpGenerator.generate(6,{digits:true,lowerCaseAlphabets:false,upperCaseAlphabets:false,specialChars:false});
    console.log(OTP);
    const number=req.body.phone;
    const otp=new model.otpModel({contact:number, otp: OTP});
    const result=await otp.save();
    res.status(200).send("OTP sent successfully"); 
}


async function verifyotp(req,res,next){
    console.log(req.body,'   ',req.body.phone);
    const sentotp = await model.otpModel.findOne({phone:req.body.phone});
    
console.log(sentotp.otp,'===',req.body.otp);
if(sentotp){
if(sentotp.otp===req.body.otp)
{
    const user=new model.adminModel({phone:req.body.phone})
    const check=await model.adminModel.findOne({phone:req.body.phone})
    if(check){
        res.status(401).send("user already exists");
    }
    else{
        const token=await user.generateToken();
        console.log(token);
        const registered=await user.save();
        console.log(registered);
        res.send({"Token":registered.tokens});
    }
}
else{
    res.status(400).send("Invalid otp")
}
}

}


async function updateprofile(req,res,next){
    let token=req.headers['x-acceess-token'];
    if(token==undefined){
        res.status(400).send({"error":"Token not found"});
    }
    if(token.startsWith('Bearer'))
    {
        token=token.slice(7,token.length);
    }
    console.log(token,"tokennn")
    const user=await model.adminModel.findOneAndUpdate({"tokens.token":token},{$set:{
            Name:req.body.Name,
    Email:req.body.Email,
    password:req.body.password,
    phone:req.body.phone,
   Role:req.body.Role
    }},{new:true});
    console.log(user,'userrrr')
    res.send(user)
}






async function addAdmin(req,res,next){
    try{
         req.body.password = await hashed.securepassword(req.body.password)
const data= await services.adminservice.addadmin(req.body);
if (data) {
            return res.status(201).send({
                message: "Product created successfully",
                data
            })
        }
        return res.send({
            message: "Something went wrong",
            status: false,
            statusCode: 500
        })
    } catch (error) {
        console.log(error);
        return res.json({
            message: error
        })
    }
    }



async function Loginadmin(req, res){
    try{
        const Email=req.body.Email
        const password=req.body.password
        const admin=await model.adminModel.findOne({email:Email,password:password})
        console.log(admin)

       if(admin.Email === req.body.Email && admin.password === req.body.password){
            console.log(admin)
            res.status(200).send({
                message: "log in successfully",
                data: admin
            })
        }else{
            res.send("details are not present")
        } 
    }catch(error){
        res.status(400).send({
            message: "Invalid details"
        })
    }
}


async function resetOtp(req,res,next){
try{
const data=await model.adminModel.findOne({phone:req.body.phone})
if(data){
    const OTP=otpGenerator.generate(6,{digits:true,lowerCaseAlphabets:false,upperCaseAlphabets:false,specialChars:false});
    console.log(OTP)
    const number=req.body.phone
    const otp=new model.otpModel({phone:number , otp:OTP})
    const result=await otp.save()
    res.send(otp)
}else{
    res.send("number not exist")
}
}catch(error){
    res.send("something went wrong")
}
}



async function resetOtpVerify(req,res,next){
    console.log(req.body,'=== ',req.body.phone);
    const sentotp = await model.otpModel.findOne({phone:req.body.phone});
    
console.log(sentotp.otp,'===',req.body.otp);
if(sentotp){
if(sentotp.otp===req.body.otp)
{
  
    const check=await model.adminModel.findOne({phone:req.body.phone})
   
    
        const token=await check.generateToken();
        console.log(token);
        const registered=await check.save();
        console.log(registered);
        res.send({"Token":registered.tokens});
    
}
else{
    res.status(400).send("Invalid otp")
}
}
}

async function Resetpassword(req,res,next){
    let token=req.headers['x-acceess-token'];
    if(token==undefined){
        res.status(400).send({"error":"Token not found"});
    }
    if(token.startsWith('Bearer'))
    {
        token=token.slice(7,token.length);
    }
    console.log(token,"tokennn")
    const user=await model.adminModel.findOneAndUpdate({"tokens.token":token},{$set:{
    password:req.body.password
    }},{new:true});
    console.log(user,'userrrr')
    res.send(user)
}

async function changePassword(req,res,next){
try{
    const user= await model.adminModel.findOne({phone:req.body.phone})
    console.log(user);
    if(user){
        console.log("aaaa")
        console.log(user.password,'===',req.body.oldPassword)
        if(user.password===req.body.oldPassword){
            const data=await model.adminModel.findOneAndUpdate({phone:req.body.phone},{
                $set:{
                    password:req.body.password
                }
            },{new:true})
            const result=data.save()
            console.log("aaaaaaasdfg")
        }else{
            res.send("Old password is not matching")
        }
    }else{
        res.send("user not exist")
    }
    res.status(200).send({
        message: "password updated successfully"
    })
}catch(error){
    console.log(error)
    res.send(error)
}
}

async function getProfile(req,res,next){
    try{
        const Email=req.body.Email
        const admin=await model.adminModel.findOne({Email:Email})
        if(!admin){
            res.send("user is not registered")
        }
        else if(admin.password != req.body.password){
            res.send("wrong password")
        }
        if(admin.Email === req.body.Email && admin.password === req.body.password){
            console.log(admin)
            res.status(200).send({
                message: "login successfully",
                data: admin
            })
        }else{
            res.send("details are not present")
        }
    }catch(error){
        res.status(400).send({
            message: "Invalid details"
        })
    }
}





module.exports={
    addAdmin,
    Loginadmin, 
    sendotp, 
    verifyotp, 
    updateprofile,
    Resetpassword,
    resetOtp,
    resetOtpVerify,
    changePassword,
    getProfile
}