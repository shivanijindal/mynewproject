const express = require("express");
const router = express.Router();
const controller = require("../controllers/index")



router.post("/postadmin", controller.adminController.addAdmin);
router.get("/login",controller.adminController.Loginadmin);
router.post("/sendotp",controller.adminController.sendotp);
 router.post("/verify",controller.adminController.verifyotp);
 router.post("/profile",controller.adminController.updateprofile);
 router.post("/resetpass",controller.adminController.Resetpassword);
router.post("/sendresetotp",controller.adminController.resetOtp);
router.post("/verifyotp",controller.adminController.resetOtpVerify);
router.post("/changepass",controller.adminController.changePassword);
router.get("/getprofile",controller.adminController.getProfile);
module.exports = router