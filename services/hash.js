
const bcrypt = require('bcrypt')

async function securepassword(password) {
    const salt = await bcrypt.genSalt(10);
    try {
        return await bcrypt.hash(password, salt)
    } catch (error) {
        throw error
    }
}
module.exports={
securepassword
}