const express = require("express");
require('dotenv').config();
const app = express();
const conn = require("./conn/db");
const v1Routes = require("./v1/routes/index");
app.use(express.json()); //if we send any request then it will read it in json form
app.use(express.urlencoded({extended:false}));
app.use("/v1", v1Routes)


app.use((err, req, res, next) => {
    res.status(400);
    res.send({
        message: err || err.message,
        data: {}
    })
})
app.listen(5000,()=>{
console.log("Server is running at 5000 port");
});