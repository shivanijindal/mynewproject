const mongoose=require("mongoose");
const otpModel=new mongoose.Schema({
    contact:{
        type:String
    },
    otp:{
        type:String
    },
    createdAt:{type: Date, default: Date.now,index:{expiresIn:"5min"}}
    
})

const otp=mongoose.model("otp",otpModel);
module.exports=otp;