require('dotenv').config();
const services=require("../services/jwt")
const mongoose=require("mongoose");
const studentModel=new mongoose.Schema({
    name:{
        type:String
    },
    email:{
        type:String
    },
    password:{
        type:String
    },
    phone:{
type:String
    },
    jti:{
    type: String
  },
  tokens : [{
        token : {
            type : String,
            required : true
        }
    }],
    createdAt:{
        type: Date, 
        default: Date.now,
        index:{expires:300}
    },
    isActive:{
        type:Boolean,
        default: false
    }    
})

studentModel.methods.generateToken=async function(){
    try{
         let jti = Math.random().toString(36).slice(2)+Math.random().toString(36).slice(2);
        console.log(this._id);
        const token=services.sign({_id:this._id.toString()},{$set:{jti:jti}},process.env.jwtSecret);
        this.jti=jti;
        await this.save();
        this.tokens=this.tokens.concat({token:token});
        await this.save();
       // console.log(token,'tokeeeen');
        return token;
    }catch(err){
return err;
    }
}
studentModel.pre("save",async function(next){
    if(this.isModified("password")){
    this.password=await bcrypt.hash(this.password,10);
}
next()
})
;

const student=mongoose.model("student",studentModel);
module.exports=student;